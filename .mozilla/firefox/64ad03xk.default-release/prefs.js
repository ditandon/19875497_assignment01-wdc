// Mozilla User Preferences

// DO NOT EDIT THIS FILE.
//
// If you make changes to this file while the application is running,
// the changes will be overwritten when the application exits.
//
// To change a preference value, you can either:
// - modify it via the UI (e.g. via about:config in the browser); or
// - set it within a user.js file in your profile.

user_pref("app.normandy.first_run", false);
user_pref("app.normandy.migrationsApplied", 10);
user_pref("app.normandy.user_id", "9f45a53f-1b08-4b7e-bffa-f129d17f1d20");
user_pref("app.update.lastUpdateTime.addon-background-update-timer", 1600491497);
user_pref("app.update.lastUpdateTime.browser-cleanup-thumbnails", 1600514896);
user_pref("app.update.lastUpdateTime.recipe-client-addon-run", 1600512857);
user_pref("app.update.lastUpdateTime.rs-experiment-loader-timer", 1600512737);
user_pref("app.update.lastUpdateTime.search-engine-update-timer", 1600512491);
user_pref("app.update.lastUpdateTime.services-settings-poll-changes", 1600491377);
user_pref("app.update.lastUpdateTime.telemetry_modules_ping", 1600153586);
user_pref("app.update.lastUpdateTime.xpi-signature-verification", 1600491617);
user_pref("browser.bookmarks.restore_default_bookmarks", false);
user_pref("browser.cache.disk.amount_written", 84965);
user_pref("browser.cache.disk.filesystem_reported", 1);
user_pref("browser.contentblocking.category", "standard");
user_pref("browser.laterrun.bookkeeping.profileCreationTime", 1599447817);
user_pref("browser.laterrun.bookkeeping.sessionCount", 9);
user_pref("browser.laterrun.enabled", true);
user_pref("browser.migration.version", 96);
user_pref("browser.newtabpage.activity-stream.impressionId", "{11446b4d-a4dd-4ab4-a457-a4ed64abce01}");
user_pref("browser.newtabpage.storageVersion", 1);
user_pref("browser.pageActions.persistedActions", "{\"version\":1,\"ids\":[\"bookmark\",\"pinTab\",\"bookmarkSeparator\",\"copyURL\",\"emailLink\",\"addSearchEngine\",\"sendToDevice\",\"pocket\",\"screenshots_mozilla_org\"],\"idsInUrlbar\":[\"pocket\",\"bookmark\"]}");
user_pref("browser.pagethumbnails.storage_version", 3);
user_pref("browser.rights.3.shown", true);
user_pref("browser.safebrowsing.provider.google4.lastupdatetime", "1600513749261");
user_pref("browser.safebrowsing.provider.google4.nextupdatetime", "1600515541261");
user_pref("browser.safebrowsing.provider.mozilla.lastupdatetime", "1600511848336");
user_pref("browser.safebrowsing.provider.mozilla.nextupdatetime", "1600515448336");
user_pref("browser.search.region", "AU");
user_pref("browser.sessionstore.upgradeBackup.latestBuildID", "20200818235255");
user_pref("browser.shell.mostRecentDateSetAsDefault", "1600501050");
user_pref("browser.slowStartup.averageTime", 170199);
user_pref("browser.slowStartup.samples", 4);
user_pref("browser.startup.homepage_override.buildID", "20200831163820");
user_pref("browser.startup.homepage_override.mstone", "80.0.1");
user_pref("browser.startup.lastColdStartupCheck", 1600500495);
user_pref("browser.uiCustomization.state", "{\"placements\":{\"widget-overflow-fixed-list\":[],\"nav-bar\":[\"back-button\",\"forward-button\",\"stop-reload-button\",\"home-button\",\"customizableui-special-spring1\",\"urlbar-container\",\"customizableui-special-spring2\",\"downloads-button\",\"library-button\",\"sidebar-button\",\"fxa-toolbar-menu-button\"],\"toolbar-menubar\":[\"menubar-items\"],\"TabsToolbar\":[\"tabbrowser-tabs\",\"new-tab-button\",\"alltabs-button\"],\"PersonalToolbar\":[\"personal-bookmarks\"]},\"seen\":[\"developer-button\"],\"dirtyAreaCache\":[\"nav-bar\",\"toolbar-menubar\",\"TabsToolbar\",\"PersonalToolbar\"],\"currentVersion\":16,\"newElementCount\":2}");
user_pref("browser.urlbar.placeholderName", "Google");
user_pref("browser.urlbar.tipShownCount.searchTip_onboard", 4);
user_pref("datareporting.policy.dataSubmissionPolicyAcceptedVersion", 2);
user_pref("datareporting.policy.dataSubmissionPolicyNotifiedTime", "1599447824455");
user_pref("distribution.canonical.bookmarksProcessed", true);
user_pref("distribution.iniFile.exists.appversion", "80.0.1");
user_pref("distribution.iniFile.exists.value", true);
user_pref("doh-rollout.balrog-migration-done", true);
user_pref("doh-rollout.doneFirstRun", true);
user_pref("dom.push.userAgentID", "91400024744b4cd8badd7c257aa26bdb");
user_pref("extensions.activeThemeID", "default-theme@mozilla.org");
user_pref("extensions.blocklist.pingCountVersion", -1);
user_pref("extensions.databaseSchema", 32);
user_pref("extensions.getAddons.cache.lastUpdate", 1600491498);
user_pref("extensions.getAddons.databaseSchema", 6);
user_pref("extensions.incognito.migrated", true);
user_pref("extensions.lastAppBuildId", "20200831163820");
user_pref("extensions.lastAppVersion", "80.0.1");
user_pref("extensions.lastPlatformVersion", "80.0.1");
user_pref("extensions.pendingOperations", false);
user_pref("extensions.systemAddonSet", "{\"schema\":1,\"addons\":{}}");
user_pref("extensions.webcompat.perform_injections", true);
user_pref("extensions.webcompat.perform_ua_overrides", true);
user_pref("extensions.webextensions.ExtensionStorageIDB.migrated.screenshots@mozilla.org", true);
user_pref("extensions.webextensions.uuids", "{\"doh-rollout@mozilla.org\":\"299c7ab3-cc10-4e59-acf8-e7f2767bede3\",\"formautofill@mozilla.org\":\"c030bb21-7f5e-4cf1-b19a-bac56e478b7a\",\"screenshots@mozilla.org\":\"a8d291cf-5a0d-42b3-8c22-bb55c7492a6e\",\"webcompat-reporter@mozilla.org\":\"7a1dd291-c6c4-4297-83ea-204a72acb90d\",\"webcompat@mozilla.org\":\"afb29e39-76d0-40b8-af4c-cb0fe423203f\",\"default-theme@mozilla.org\":\"1e0c85d7-8008-45f6-88a6-afc5c1a39c26\",\"google@search.mozilla.org\":\"1a24e782-f08c-499d-b9b6-fcfba56d0ef3\",\"wikipedia@search.mozilla.org\":\"be73dfc8-4abb-4fef-8b65-09ad75b84694\",\"bing@search.mozilla.org\":\"f289182c-9feb-44a1-aaa7-47e0159df77d\",\"ddg@search.mozilla.org\":\"8d43ed3a-9311-4af0-99e7-e38418bbcc2c\",\"amazon@search.mozilla.org\":\"fd7c69af-a697-4112-9a50-fe26a5df8628\",\"ebay@search.mozilla.org\":\"decf07b1-6bd0-4684-91a1-32e8c5b4f09c\"}");
user_pref("gfx.blacklist.layers.opengl", 4);
user_pref("gfx.blacklist.layers.opengl.failureid", "FEATURE_FAILURE_SOFTWARE_GL");
user_pref("idle.lastDailyNotification", 1600491773);
user_pref("media.gmp-gmpopenh264.abi", "x86_64-gcc3");
user_pref("media.gmp-gmpopenh264.lastUpdate", 1599448066);
user_pref("media.gmp-gmpopenh264.version", "1.8.1.1");
user_pref("media.gmp-manager.buildID", "20200831163820");
user_pref("media.gmp-manager.lastCheck", 1600491057);
user_pref("media.gmp.storage.version.observed", 1);
user_pref("pdfjs.enabledCache.state", true);
user_pref("pdfjs.migrationVersion", 2);
user_pref("places.database.lastMaintenance", 1600152951);
user_pref("places.history.expiration.transient_current_max_pages", 112348);
user_pref("privacy.purge_trackers.date_in_cookie_database", "0");
user_pref("privacy.sanitize.pending", "[{\"id\":\"newtab-container\",\"itemsToClear\":[],\"options\":{}}]");
user_pref("security.remote_settings.crlite_filters.checked", 1600491377);
user_pref("security.remote_settings.intermediates.checked", 1600491377);
user_pref("security.sandbox.content.tempDirSuffix", "57809b0f-ea90-4947-a011-7de57429cfb1");
user_pref("security.sandbox.plugin.tempDirSuffix", "a10dba81-31d0-4c25-8d2b-4cdc71edfc70");
user_pref("services.blocklist.addons-mlbf.checked", 1600491377);
user_pref("services.blocklist.gfx.checked", 1600491377);
user_pref("services.blocklist.pinning.checked", 1600491377);
user_pref("services.blocklist.plugins.checked", 1600491377);
user_pref("services.settings.clock_skew_seconds", 0);
user_pref("services.settings.last_etag", "\"1600462879095\"");
user_pref("services.settings.last_update_seconds", 1600491377);
user_pref("services.settings.main.anti-tracking-url-decoration.last_check", 1600491377);
user_pref("services.settings.main.cfr-fxa.last_check", 1600491377);
user_pref("services.settings.main.cfr.last_check", 1600491377);
user_pref("services.settings.main.fxmonitor-breaches.last_check", 1600491377);
user_pref("services.settings.main.hijack-blocklists.last_check", 1600491377);
user_pref("services.settings.main.language-dictionaries.last_check", 1600491377);
user_pref("services.settings.main.message-groups.last_check", 1600491377);
user_pref("services.settings.main.messaging-experiments.last_check", 1600491377);
user_pref("services.settings.main.normandy-recipes-capabilities.last_check", 1600491377);
user_pref("services.settings.main.partitioning-exempt-urls.last_check", 1600491377);
user_pref("services.settings.main.pioneer-study-addons.last_check", 1600491377);
user_pref("services.settings.main.public-suffix-list.last_check", 1600491377);
user_pref("services.settings.main.search-config.last_check", 1600491377);
user_pref("services.settings.main.search-default-override-allowlist.last_check", 1600491377);
user_pref("services.settings.main.sites-classification.last_check", 1600491377);
user_pref("services.settings.main.tippytop.last_check", 1600491377);
user_pref("services.settings.main.url-classifier-skip-urls.last_check", 1600491377);
user_pref("services.settings.main.whats-new-panel.last_check", 1600491377);
user_pref("services.settings.security.onecrl.checked", 1600491377);
user_pref("storage.vacuum.last.index", 1);
user_pref("storage.vacuum.last.places.sqlite", 1600152951);
user_pref("toolkit.startup.last_success", 1600500436);
user_pref("toolkit.telemetry.cachedClientID", "82e9f1f2-d08e-4ed2-b2ef-74771b539621");
user_pref("toolkit.telemetry.pioneer-new-studies-available", true);
user_pref("toolkit.telemetry.previousBuildID", "20200831163820");
user_pref("toolkit.telemetry.reportingpolicy.firstRun", false);
user_pref("trailhead.firstrun.didSeeAboutWelcome", true);
